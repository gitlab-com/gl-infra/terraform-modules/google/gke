locals {
  node_pools_linux_node_configs_sysctls = merge(
    { all = {} },
    { default-node-pool = {} },
    zipmap(
      [for name in keys(var.node_pools) : name],
      [for node_pool in var.node_pools : {}]
    ),
    var.node_pools_linux_node_configs_sysctls
  )

  # So we need to somehow ensure backward compatability with this module.  To
  # do this, we need to ensure the `google_container_cluster.location` object
  # has the correct information:
  #   * For a zonal cluster, location should be set to a single zone
  #   * For a regional cluster, location should be set to the region
  # Take all known zones as list, and transform it to a string separated by ","
  # Example:
  #   Input:
  #     [
  #       "us-east1-a",
  #       "us-east1-b",
  #       "us-east1-c"
  #     ]
  #
  #  Becomes:
  #    "us-east1-a,us-east1-b,us-east1-c"
  string_all_node_locations = length(var.regional_cluster_zones) > 0 ? join(",", var.regional_cluster_zones) : join(",", data.google_compute_zones.available.names)

  # Replace what we have historically used as the master zone, and replace it when an empty string
  # Example:
  #   Input:
  #    "us-east1-a,us-east1-b,us-east1-c"
  #  Becomes:
  #    ",us-east1-b,us-east1-c"
  no_primary_node_locations = replace(
    local.string_all_node_locations,
    data.google_compute_zones.available.names[0],
    "",
  )

  # Set which string we want to use based on whether or not this is a regional or zonal cluster:
  #   * For a zonal cluster, it should resemble: ",us-east1-b,us-east1-c"
  #   * For a regional cluster, it will be all available zones in the region unless var.regional_cluster_zones is explicitly set.
  determine_node_locations = var.regional_cluster ? local.string_all_node_locations : local.no_primary_node_locations

  # Now take that string, turn it back into a list
  # Example:
  #   Input:
  #    ",us-east1-b,us-east1-c"
  #
  #  Becomes:
  #     [
  #       "",
  #       "us-east1-b",
  #       "us-east1-c"
  #     ]
  clean_up_node_locations = split(",", local.determine_node_locations)

  # Remove any empty objects from that list
  # Example:
  #   Input:
  #     [
  #       "",
  #       "us-east1-b",
  #       "us-east1-c"
  #     ]
  #
  #  Becomes:
  #     [
  #       "us-east1-b",
  #       "us-east1-c"
  #     ]
  finalize_node_locations = compact(local.clean_up_node_locations)

  zone = var.zone == "" ? data.google_compute_zones.available.names[0] : var.zone

  cluster_network_policy = var.network_policy ? [{
    enabled  = true
    provider = var.network_policy_provider
    }] : [{
    enabled  = false
    provider = null
  }]
  cluster_gce_pd_csi_config = var.gce_pd_csi_driver ? [{ enabled = true }] : [{ enabled = false }]

  cluster_name = var.name != null ? format("%v-%v", var.environment, var.name) : format("%v-%v", var.name_prefix, random_string.cluster_id[0].result)
}

resource "random_string" "cluster_id" {
  count = var.name_prefix != null ? 1 : 0

  length  = 5
  numeric = true
  lower   = true
  upper   = false
  special = false
}

resource "google_container_cluster" "cluster" {
  provider = google-beta

  project = var.project
  name    = local.cluster_name

  location           = var.regional_cluster ? var.region : local.zone
  node_locations     = var.zone != "" ? null : local.finalize_node_locations
  initial_node_count = 1

  min_master_version       = var.kubernetes_version == "" ? null : var.kubernetes_version
  remove_default_node_pool = true
  enable_shielded_nodes    = var.enable_shielded_nodes

  network    = var.vpc
  subnetwork = google_compute_subnetwork.gke.name

  gateway_api_config {
    channel = "CHANNEL_STANDARD"
  }

  dynamic "network_policy" {
    for_each = local.cluster_network_policy

    content {
      enabled  = network_policy.value.enabled
      provider = network_policy.value.provider
    }
  }

  addons_config {
    http_load_balancing {
      disabled = !var.http_load_balancing
    }

    horizontal_pod_autoscaling {
      disabled = !var.horizontal_pod_autoscaling
    }

    network_policy_config {
      disabled = !var.network_policy
    }

    dynamic "gce_persistent_disk_csi_driver_config" {
      for_each = local.cluster_gce_pd_csi_config

      content {
        enabled = gce_persistent_disk_csi_driver_config.value.enabled
      }
    }

    config_connector_config {
      enabled = var.config_connector
    }
  }

  datapath_provider = var.datapath_provider

  networking_mode = "VPC_NATIVE"
  ip_allocation_policy {
    cluster_secondary_range_name  = google_compute_subnetwork.gke.secondary_ip_range[0].range_name
    services_secondary_range_name = google_compute_subnetwork.gke.secondary_ip_range[1].range_name
  }

  private_cluster_config {
    enable_private_nodes    = var.private_cluster
    enable_private_endpoint = var.private_cluster_endpoint
    master_ipv4_cidr_block  = var.private_cluster ? var.private_master_cidr : ""
  }

  default_snat_status {
    disabled = var.disable_default_snat
  }

  master_authorized_networks_config {
    dynamic "cidr_blocks" {
      for_each = var.authorized_master_access
      content {
        cidr_block = cidr_blocks.value
      }
    }
  }

  vertical_pod_autoscaling {
    enabled = var.enable_vertical_pod_autoscaling
  }

  enable_l4_ilb_subsetting = var.enable_l4_ilb_subsetting

  dynamic "workload_identity_config" {
    for_each = range(var.enable_workload_identity ? 1 : 0)
    content {
      workload_pool = format("%s.svc.id.goog", var.project)
    }
  }

  resource_labels = merge(
    {
      # Let's set a label on the cluster to let us know whether or not our
      # cluster is private or not.
      cluster_type = var.private_cluster ? "private" : "public"
      cluster_loc  = var.regional_cluster ? "regional" : "zonal"
    },
    var.cluster_resource_labels,
  )

  dynamic "logging_config" {
    for_each = length(var.logging_enabled_components) > 0 ? [1] : []

    content {
      enable_components = var.logging_enabled_components
    }
  }

  dynamic "monitoring_config" {
    for_each = length(var.monitoring_enabled_components) > 0 || var.monitoring_enable_managed_prometheus ? [1] : []

    content {
      enable_components = length(var.monitoring_enabled_components) > 0 ? var.monitoring_enabled_components : null

      dynamic "managed_prometheus" {
        for_each = var.monitoring_enable_managed_prometheus ? [1] : []

        content {
          enabled = var.monitoring_enable_managed_prometheus
        }
      }
    }
  }

  # Resource Usage Metering
  # GCP: https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-usage-metering
  # Terraform: https://www.terraform.io/docs/providers/google/r/container_cluster.html#resource_usage_export_config
  dynamic "resource_usage_export_config" {
    for_each = var.network_egress_metering_enabled || var.resource_consumption_metering_enabled == true ? [1] : []
    content {
      enable_network_egress_metering       = var.network_egress_metering_enabled
      enable_resource_consumption_metering = var.resource_consumption_metering_enabled

      bigquery_destination {
        dataset_id = var.resource_usage_dataset
      }
    }
  }

  dynamic "maintenance_policy" {
    for_each = var.maintenance_policy == null ? [] : [var.maintenance_policy]
    content {
      recurring_window {
        start_time = maintenance_policy.value.start_time
        end_time   = maintenance_policy.value.end_time
        recurrence = maintenance_policy.value.recurrence
      }

      dynamic "maintenance_exclusion" {
        for_each = var.maintenance_exclusions
        content {
          exclusion_name = maintenance_exclusion.value.name
          start_time     = maintenance_exclusion.value.start_time
          end_time       = maintenance_exclusion.value.end_time
        }
      }
    }
  }

  dynamic "release_channel" {
    for_each = var.release_channel == "UNSPECIFIED" ? [] : [var.release_channel]
    content {
      channel = release_channel.value
    }
  }

  notification_config {
    pubsub {
      enabled = var.upgrade_notification_queue == null ? false : true
      topic   = var.upgrade_notification_queue
    }
  }

  dynamic "cost_management_config" {
    for_each = var.enable_cost_allocation ? [1] : []
    content {
      enabled = var.enable_cost_allocation
    }
  }
}

resource "google_container_node_pool" "node_pool" {
  provider = google-beta

  for_each = var.node_pools

  name               = each.key
  project            = var.project
  location           = var.regional_cluster ? var.region : local.zone
  cluster            = google_container_cluster.cluster.name
  node_locations     = each.value.node_locations
  initial_node_count = each.value.initial_node_count

  version = each.value.node_auto_upgrade ? null : coalesce(each.value.kubernetes_version, var.kubernetes_version)

  autoscaling {
    min_node_count = each.value.min_node_count
    max_node_count = each.value.max_node_count
    # https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-autoscaler#location_policy
    location_policy = each.value.location_policy
  }

  management {
    auto_repair  = each.value.node_auto_repair
    auto_upgrade = each.value.node_auto_upgrade
  }

  upgrade_settings {
    max_surge       = each.value.max_surge
    max_unavailable = each.value.max_unavailable
  }

  node_config {
    image_type   = each.value.node_image_type
    disk_size_gb = each.value.node_disk_size_gb
    disk_type    = each.value.node_disk_type

    spot         = each.value.spot
    machine_type = each.value.machine_type

    oauth_scopes = coalescelist(each.value.oauth_scopes, var.oauth_scopes)

    service_account = coalesce(var.service_account, google_service_account.gke-nodes.email)

    labels = merge({
      environment = var.environment
      name        = local.cluster_name
      pool        = each.key
      spot        = each.value.spot
      shard       = each.value.shard
      stage       = each.value.stage
      type        = each.value.type
      zone        = var.zone != "" ? var.zone : null
      },
      each.value.labels,
    )

    tags = [
      "gke-node",
      local.cluster_name,
      var.environment,
      each.key,
    ]

    metadata = {
      disable-legacy-endpoints = true
    }

    dynamic "workload_metadata_config" {
      for_each = range(coalesce(each.value.enable_workload_identity, var.enable_workload_identity) ? 1 : 0)

      content {
        mode = "GKE_METADATA"
      }
    }

    dynamic "taint" {
      for_each = each.value.taints

      content {
        key    = taint.value.key
        value  = taint.value.value
        effect = taint.value.effect
      }
    }

    dynamic "kubelet_config" {
      for_each = range(each.value.cpu_manager_policy != null || each.value.cpu_cfs_quota != null || each.value.cpu_cfs_quota_period != null ? 1 : 0)

      content {
        cpu_manager_policy   = coalesce(each.value.cpu_manager_policy, "static")
        cpu_cfs_quota        = each.value.cpu_cfs_quota
        cpu_cfs_quota_period = each.value.cpu_cfs_quota_period
      }
    }

    dynamic "linux_node_config" {
      for_each = length(merge(
        local.node_pools_linux_node_configs_sysctls["all"],
        local.node_pools_linux_node_configs_sysctls[each.key]
      )) != 0 ? [1] : []

      content {
        sysctls = merge(
          local.node_pools_linux_node_configs_sysctls["all"],
          local.node_pools_linux_node_configs_sysctls[each.key]
        )
      }
    }
  }

  lifecycle {
    ignore_changes = [
      # Ignore node count changes as we use auto-scaling
      initial_node_count
    ]
  }
}
