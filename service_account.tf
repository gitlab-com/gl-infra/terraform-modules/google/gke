# This is a service account that contains the minimal possible permissions
# required for nodes to operate inside of GKE
# https://cloud.google.com/kubernetes-engine/docs/how-to/hardening-your-cluster

resource "google_service_account" "gke-nodes" {
  account_id   = coalesce(var.nodes_service_account_name, format("gke-%v-nodes", local.cluster_name))
  display_name = format("%v-nodes", local.cluster_name)
  project      = var.project
}

resource "google_project_iam_member" "gke-loggingWriter" {
  project = var.project
  role    = "roles/logging.logWriter"
  member  = google_service_account.gke-nodes.member
}

resource "google_project_iam_member" "gke-metricWriter" {
  project = var.project
  role    = "roles/monitoring.metricWriter"
  member  = google_service_account.gke-nodes.member
}

resource "google_project_iam_member" "gke-monitoringViewer" {
  project = var.project
  role    = "roles/monitoring.viewer"
  member  = google_service_account.gke-nodes.member
}

###
#
# Creates GSA for use with Workload Identity
# Reference: https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity
#
# var.workload_identity_ksa will contain a data structure that looks like:
# {
#   "fooo/barrrr" = ["roles/cloudprofiler.agent", "roles/logging.logWriter"],
#   "bazz/batttt" = ["roles/monitoring.metricWriter"]
# }
#
# Where `fooo/barrrr` and `bazz/batttt` are Kubernetes Service Accounts
#   and the mapped array are the roles that we'd like their corresponding
#   GCP Service Account to be provided access to
#
# *WARNING* All provided roles will be added to the appropriate
#   Workload Identity Role in GCP

locals {

  # This converts the var.workload_identity_ksa data structure into
  # a format which terraform can consume later
  # Example:
  #  Output:
  #  {
  #    "bazz/batttt_roles/monitoring.metricWriter": {
  #      "ksa" = "bazz/batttt"
  #      "role" = "roles/monitoring.metricWriter"
  #    },
  #    "fooo/barrrr_roles/cloudprofiler.agent": {
  #      "ksa" = "fooo/barrrr"
  #      "role" = "roles/cloudprofiler.agent"
  #    },
  #  }
  roles = {
    for kroles in flatten([
      for ksa, roles in var.workload_identity_ksa : [
        for role in roles : [
          {
            ksa  = ksa
            role = role
          }
        ]
      ]
      ]) : "${kroles.ksa}_${kroles.role}" => {
      ksa  = kroles.ksa
      role = kroles.role
    }
  }

  gsa_account_ids = {
    for ksa in keys(var.workload_identity_ksa) :
    ksa => replace(ksa, "/.*/([[:alnum:]]+)/", "$1")
  }
}

# Create the GSA
# This ID will only contain the latter half of the KSA
# Using regex, we strip off everything in front of the first `/`
# Example:
#   * "batttt"
#   * "barrrr"
resource "google_service_account" "gsa" {
  for_each = local.gsa_account_ids

  account_id   = each.value
  display_name = each.key
  description  = "Terraform created Google Service Account for the Kubernetes Service Account ${each.key}"
  project      = var.project
}

# IAM bind policy to add a given KSA to the GSA to utilize GKE Workload Identity
resource "google_service_account_iam_member" "workload-identity" {
  for_each = var.workload_identity_ksa

  role               = "roles/iam.workloadIdentityUser"
  service_account_id = google_service_account.gsa[each.key].name
  member             = "serviceAccount:${var.project}.svc.id.goog[${each.key}]"

  depends_on = [
    google_container_cluster.cluster
  ]
}

# Grant the requested roles to the Google Service Account
resource "google_project_iam_member" "ksa-to-gcp-role" {
  for_each = local.roles

  project = var.project
  role    = each.value.role
  member  = google_service_account.gsa[each.value.ksa].member
}
