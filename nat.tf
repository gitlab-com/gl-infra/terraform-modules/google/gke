# IP address for NAT
locals {
  nat_name = coalesce(var.subnet_nat_name, format("gke-%v-%v", local.cluster_name, var.environment))
}

resource "google_compute_address" "gke-cloud-nat-ip" {
  count       = var.create_nat ? 1 : 0
  name        = local.nat_name
  description = "GKE NAT IP"
}

resource "google_compute_router" "nat-router" {
  count   = var.create_nat ? 1 : 0
  name    = local.nat_name
  network = var.vpc
}

resource "google_compute_router_nat" "gke-nat" {
  count                              = var.create_nat ? 1 : 0
  name                               = local.nat_name
  router                             = google_compute_router.nat-router[0].name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.gke-cloud-nat-ip[0].self_link]
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = google_compute_subnetwork.gke.self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}
