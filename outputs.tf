output "cluster_endpoint" {
  value       = google_container_cluster.cluster.endpoint
  description = "Cluster endpoint"
}

output "cluster_name" {
  value       = local.cluster_name
  description = "Cluster name"
}

output "cluster_id" {
  value       = google_container_cluster.cluster.id
  description = "Cluster ID"
}

output "region" {
  value       = var.region
  description = "Cluster region"
}

output "gateway_address" {
  value       = google_compute_subnetwork.gke[*].gateway_address
  description = "The IP address of the gateway"
}

output "subnetwork_self_link" {
  value       = google_compute_subnetwork.gke.self_link
  description = "The URL of the created subnetwork"
}

output "cluster_ca_certificate" {
  value       = google_container_cluster.cluster.master_auth[0].cluster_ca_certificate
  description = "The CA certificate used by the API servers of the Kubernetes cluster"
}

output "workload_identity_service_account_emails" {
  value = {
    for ksa, gsa_account_id in local.gsa_account_ids :
    ksa => format("%s@%s.iam.gserviceaccount.com", gsa_account_id, var.project)
  }
  description = "The emails of the Google Service Accounts created for each Kubernetes Service Account"
}
