# GitLab.com GKE Terraform Module

## What is this?

This module provisions a GKE cluster.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.5 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 3.90 |
| <a name="requirement_google-beta"></a> [google-beta](#requirement\_google-beta) | >= 3.90 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.5 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 3.90 |
| <a name="provider_google-beta"></a> [google-beta](#provider\_google-beta) | >= 3.90 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 3.5 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google-beta_google_container_cluster.cluster](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_container_cluster) | resource |
| [google-beta_google_container_node_pool.node_pool](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_container_node_pool) | resource |
| [google_compute_address.gke-cloud-nat-ip](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_firewall.gke-master-to-kubelet](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.gke-ssh-access](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_router.nat-router](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_router) | resource |
| [google_compute_router_nat.gke-nat](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_router_nat) | resource |
| [google_compute_subnetwork.gke](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_project_iam_member.gke-loggingWriter](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.gke-metricWriter](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.gke-monitoringViewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.ksa-to-gcp-role](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_service_account.gke-nodes](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account.gsa](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account_iam_member.workload-identity](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_iam_member) | resource |
| [random_string.cluster_id](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [google_compute_zones.available](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_authorized_master_access"></a> [authorized\_master\_access](#input\_authorized\_master\_access) | External networks that can access the Kubernetes cluster master. | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_cloud_iap_netblock"></a> [cloud\_iap\_netblock](#input\_cloud\_iap\_netblock) | IP range of the IAP. | `string` | `"35.235.240.0/20"` | no |
| <a name="input_cluster_resource_labels"></a> [cluster\_resource\_labels](#input\_cluster\_resource\_labels) | The GCE resource labels (a map of key/value pairs) to be applied to the cluster | `map(string)` | `{}` | no |
| <a name="input_config_connector"></a> [config\_connector](#input\_config\_connector) | (Beta) Whether ConfigConnector is enabled for this cluster. | `bool` | `false` | no |
| <a name="input_create_kublet_firewall"></a> [create\_kublet\_firewall](#input\_create\_kublet\_firewall) | Create firewall to allow GKE to talk to the Prometheus operator. | `bool` | `false` | no |
| <a name="input_create_nat"></a> [create\_nat](#input\_create\_nat) | Whether or not to create a NAT. | `bool` | `false` | no |
| <a name="input_datapath_provider"></a> [datapath\_provider](#input\_datapath\_provider) | The desired datapath provider for this cluster. By default, `DATAPATH_PROVIDER_UNSPECIFIED` enables the IPTables-based kube-proxy implementation. `ADVANCED_DATAPATH` enables Dataplane-V2 feature. | `string` | `"DATAPATH_PROVIDER_UNSPECIFIED"` | no |
| <a name="input_disable_default_snat"></a> [disable\_default\_snat](#input\_disable\_default\_snat) | Whether to disable the default SNAT to support the private use of public IP addresses | `bool` | `false` | no |
| <a name="input_enable_cloud_iap_access"></a> [enable\_cloud\_iap\_access](#input\_enable\_cloud\_iap\_access) | Determines whether to create a firewall rule for TCP forwarding from IAP. | `bool` | `true` | no |
| <a name="input_enable_cost_allocation"></a> [enable\_cost\_allocation](#input\_enable\_cost\_allocation) | Enables Cost Allocation Feature and the cluster name and namespace of your GKE workloads appear in the labels field of the billing export to BigQuery | `bool` | `false` | no |
| <a name="input_enable_l4_ilb_subsetting"></a> [enable\_l4\_ilb\_subsetting](#input\_enable\_l4\_ilb\_subsetting) | Enable L4 ILB Subsetting on the cluster | `bool` | `false` | no |
| <a name="input_enable_shielded_nodes"></a> [enable\_shielded\_nodes](#input\_enable\_shielded\_nodes) | Enable Shielded Nodes features on all nodes in this cluster. | `bool` | `true` | no |
| <a name="input_enable_vertical_pod_autoscaling"></a> [enable\_vertical\_pod\_autoscaling](#input\_enable\_vertical\_pod\_autoscaling) | Enable GKE vertical pod autoscaling. See https://cloud.google.com/kubernetes-engine/docs/concepts/verticalpodautoscaler | `bool` | `true` | no |
| <a name="input_enable_workload_identity"></a> [enable\_workload\_identity](#input\_enable\_workload\_identity) | Enable GKE Workload Identity support on cluster. See https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity | `bool` | `true` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name. | `string` | n/a | yes |
| <a name="input_gce_pd_csi_driver"></a> [gce\_pd\_csi\_driver](#input\_gce\_pd\_csi\_driver) | Whether this cluster should enable the Google Compute Engine Persistent Disk Container Storage Interface (CSI) Driver. https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/gce-pd-csi-driver | `bool` | `false` | no |
| <a name="input_horizontal_pod_autoscaling"></a> [horizontal\_pod\_autoscaling](#input\_horizontal\_pod\_autoscaling) | Enable horizontal pod autoscaling addon | `bool` | `true` | no |
| <a name="input_http_load_balancing"></a> [http\_load\_balancing](#input\_http\_load\_balancing) | Enable http load balancer addon | `bool` | `true` | no |
| <a name="input_ip_cidr_range"></a> [ip\_cidr\_range](#input\_ip\_cidr\_range) | The IP range for the cluster. | `string` | n/a | yes |
| <a name="input_kubelet_firewall_tags"></a> [kubelet\_firewall\_tags](#input\_kubelet\_firewall\_tags) | Extra firewall target tags for inbound control plane and SSH traffic. | `set(string)` | `[]` | no |
| <a name="input_kubernetes_version"></a> [kubernetes\_version](#input\_kubernetes\_version) | The Kubernetes version. Defaults to latest available. | `string` | `""` | no |
| <a name="input_logging_enabled_components"></a> [logging\_enabled\_components](#input\_logging\_enabled\_components) | List of services to monitor: SYSTEM\_COMPONENTS, WORKLOADS. Empty list is default GKE configuration. | `list(string)` | `[]` | no |
| <a name="input_maintenance_exclusions"></a> [maintenance\_exclusions](#input\_maintenance\_exclusions) | List of maintenance exclusions for the cluster (up to 3). See https://www.terraform.io/docs/providers/google/r/container_cluster.html#maintenance_policy | `list(object({ name = string, start_time = string, end_time = string }))` | `[]` | no |
| <a name="input_maintenance_policy"></a> [maintenance\_policy](#input\_maintenance\_policy) | The maintenance policy for the cluster. See https://www.terraform.io/docs/providers/google/r/container_cluster.html#maintenance_policy | <pre>object({<br>    start_time = string<br>    end_time   = string<br>    recurrence = string<br>  })</pre> | `null` | no |
| <a name="input_monitoring_enable_managed_prometheus"></a> [monitoring\_enable\_managed\_prometheus](#input\_monitoring\_enable\_managed\_prometheus) | (Beta) Configuration for Managed Service for Prometheus. Whether or not the managed collection is enabled. | `bool` | `false` | no |
| <a name="input_monitoring_enabled_components"></a> [monitoring\_enabled\_components](#input\_monitoring\_enabled\_components) | List of services to monitor: SYSTEM\_COMPONENTS, WORKLOADS (provider version >= 3.89.0). Empty list is default GKE configuration. | `list(string)` | `[]` | no |
| <a name="input_name"></a> [name](#input\_name) | The GKE cluster name, `name` or `name_prefix` required. | `string` | `null` | no |
| <a name="input_name_prefix"></a> [name\_prefix](#input\_name\_prefix) | The GKE cluster name prefix, conflicts with `name`, generates a random cluster name `<name_prefix>-<random_id>`. | `string` | `null` | no |
| <a name="input_network_egress_metering_enabled"></a> [network\_egress\_metering\_enabled](#input\_network\_egress\_metering\_enabled) | Whether to enable network egress metering for this cluster; requires `resource_usage_dataset` to be configured. | `bool` | `false` | no |
| <a name="input_network_policy"></a> [network\_policy](#input\_network\_policy) | Enable network policy addon | `bool` | `true` | no |
| <a name="input_network_policy_provider"></a> [network\_policy\_provider](#input\_network\_policy\_provider) | The network policy provider. | `string` | `"CALICO"` | no |
| <a name="input_node_pools"></a> [node\_pools](#input\_node\_pools) | Map of node pools with their configuration. | <pre>map(object({<br>    cpu_cfs_quota            = optional(bool)<br>    cpu_cfs_quota_period     = optional(string)<br>    cpu_manager_policy       = optional(string)<br>    enable_workload_identity = optional(bool)<br>    node_locations           = optional(list(string))<br>    initial_node_count       = optional(number, 1)<br>    kubernetes_version       = optional(string)<br>    labels                   = optional(map(string), {})<br>    location_policy          = optional(string)<br>    machine_type             = optional(string, "n2d-standard-8")<br>    max_node_count           = optional(number, 10)<br>    max_surge                = optional(number, 1)<br>    max_unavailable          = optional(number, 0)<br>    min_node_count           = optional(number, 1)<br>    node_auto_repair         = optional(bool, true)<br>    node_auto_upgrade        = optional(bool, false)<br>    node_disk_size_gb        = optional(number, 100)<br>    node_disk_type           = optional(string, "pd-ssd")<br>    node_image_type          = optional(string, "COS_CONTAINERD")<br>    oauth_scopes             = optional(list(string), [])<br>    shard                    = optional(string)<br>    spot                     = optional(bool, false)<br>    stage                    = optional(string)<br>    taints                   = optional(list(object({ key = string, value = string, effect = string })), [])<br>    type                     = optional(string, "default")<br>  }))</pre> | <pre>{<br>  "default": {}<br>}</pre> | no |
| <a name="input_node_pools_linux_node_configs_sysctls"></a> [node\_pools\_linux\_node\_configs\_sysctls](#input\_node\_pools\_linux\_node\_configs\_sysctls) | Map of maps containing linux node config sysctls by node-pool name | `map(map(string))` | <pre>{<br>  "all": {},<br>  "default-node-pool": {}<br>}</pre> | no |
| <a name="input_nodes_service_account_name"></a> [nodes\_service\_account\_name](#input\_nodes\_service\_account\_name) | Name of the gke nodes service account that is created. Defaults to `gke-{local.cluster_name}-nodes`. | `string` | `null` | no |
| <a name="input_oauth_scopes"></a> [oauth\_scopes](#input\_oauth\_scopes) | Default set of Google API scopes to enable in all node pools. | `list(string)` | <pre>[<br>  "https://www.googleapis.com/auth/compute",<br>  "https://www.googleapis.com/auth/devstorage.read_only",<br>  "https://www.googleapis.com/auth/logging.write",<br>  "https://www.googleapis.com/auth/monitoring"<br>]</pre> | no |
| <a name="input_pod_ip_cidr_range"></a> [pod\_ip\_cidr\_range](#input\_pod\_ip\_cidr\_range) | Secondary IP range for pods. /16 == /24 per node = 256 nodes | `string` | `"10.12.0.0/16"` | no |
| <a name="input_private_cluster"></a> [private\_cluster](#input\_private\_cluster) | Whether or not a cluster should be private or not. | `bool` | `true` | no |
| <a name="input_private_cluster_endpoint"></a> [private\_cluster\_endpoint](#input\_private\_cluster\_endpoint) | When true, the cluster's private endpoint is used as the cluster endpoint and access through the public endpoint is disabled. When false, either endpoint can be used. | `bool` | `false` | no |
| <a name="input_private_master_cidr"></a> [private\_master\_cidr](#input\_private\_master\_cidr) | Master IP range is a private RFC 1918 range for the master's VPC. The master range must not overlap with any subnet in your cluster's VPC. The master and your cluster use VPC peering to communicate privately. | `string` | `"172.16.0.0/28"` | no |
| <a name="input_project"></a> [project](#input\_project) | The project name. | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The target region. | `string` | n/a | yes |
| <a name="input_regional_cluster"></a> [regional\_cluster](#input\_regional\_cluster) | Creates a cluster with multiple masters spread across zones in the region; forces new resource when changed. | `bool` | `true` | no |
| <a name="input_regional_cluster_zones"></a> [regional\_cluster\_zones](#input\_regional\_cluster\_zones) | For a regional cluster, specify an explicit list of zones to place nodes. An empty list will default to all available zones. | `list(string)` | `[]` | no |
| <a name="input_release_channel"></a> [release\_channel](#input\_release\_channel) | The release channel of this cluster. Accepted values are UNSPECIFIED, RAPID, REGULAR and STABLE. Defaults to UNSPECIFIED. | `string` | `"UNSPECIFIED"` | no |
| <a name="input_resource_consumption_metering_enabled"></a> [resource\_consumption\_metering\_enabled](#input\_resource\_consumption\_metering\_enabled) | Whether to enable resource consumption metering on this cluster; requires `resource_usage_dataset` to be configured. | `bool` | `false` | no |
| <a name="input_resource_usage_dataset"></a> [resource\_usage\_dataset](#input\_resource\_usage\_dataset) | The ID of a BigQuery Dataset, required if resource usage is enabled. | `string` | `""` | no |
| <a name="input_service_account"></a> [service\_account](#input\_service\_account) | Overrides the service account managed by this module. Will apply the designated service account defined by this variable. | `string` | `null` | no |
| <a name="input_service_ip_cidr_range"></a> [service\_ip\_cidr\_range](#input\_service\_ip\_cidr\_range) | Secondary IP range for services. /22 = 1024 service IPs | `string` | `"10.16.0.0/22"` | no |
| <a name="input_subnet_nat_name"></a> [subnet\_nat\_name](#input\_subnet\_nat\_name) | Name of the network subnet and nat object that is created. Defaults to `gke-{local.cluster_name}-{var.environment}`. | `string` | `null` | no |
| <a name="input_upgrade_notification_queue"></a> [upgrade\_notification\_queue](#input\_upgrade\_notification\_queue) | The pubsub topic to send messages to when a cluster upgrade takes place. This must be a full URL path to the topic. | `string` | `null` | no |
| <a name="input_vpc"></a> [vpc](#input\_vpc) | The target network. | `string` | n/a | yes |
| <a name="input_workload_identity_ksa"></a> [workload\_identity\_ksa](#input\_workload\_identity\_ksa) | All Kubernetes service accounts that must utilize Workload Identity. Formatted as `{'<namespace>/<service_account>': ['<iam_role>', ...], ...}`. | `map(list(string))` | `{}` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | For zonal cluster, explicitly set the zone for the cluster and node pools. | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_ca_certificate"></a> [cluster\_ca\_certificate](#output\_cluster\_ca\_certificate) | The CA certificate used by the API servers of the Kubernetes cluster |
| <a name="output_cluster_endpoint"></a> [cluster\_endpoint](#output\_cluster\_endpoint) | Cluster endpoint |
| <a name="output_cluster_id"></a> [cluster\_id](#output\_cluster\_id) | Cluster ID |
| <a name="output_cluster_name"></a> [cluster\_name](#output\_cluster\_name) | Cluster name |
| <a name="output_gateway_address"></a> [gateway\_address](#output\_gateway\_address) | The IP address of the gateway |
| <a name="output_region"></a> [region](#output\_region) | Cluster region |
| <a name="output_subnetwork_self_link"></a> [subnetwork\_self\_link](#output\_subnetwork\_self\_link) | The URL of the created subnetwork |
| <a name="output_workload_identity_service_account_emails"></a> [workload\_identity\_service\_account\_emails](#output\_workload\_identity\_service\_account\_emails) | The emails of the Google Service Accounts created for each Kubernetes Service Account |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
