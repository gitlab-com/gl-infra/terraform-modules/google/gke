resource "google_compute_firewall" "gke-ssh-access" {
  count = var.enable_cloud_iap_access ? 1 : 0

  project = var.project
  name    = format("%v-%v-ssh-access", local.cluster_name, var.environment)
  network = var.vpc

  allow {
    protocol = "tcp"
    ports    = [22]
  }

  source_ranges = [var.cloud_iap_netblock]

  target_tags = setunion(
    [local.cluster_name],
    var.kubelet_firewall_tags,
  )
}

resource "google_compute_firewall" "gke-master-to-kubelet" {
  count = var.create_kublet_firewall ? 1 : 0

  project = var.project
  name    = format("k8s-api-to-kubelets-%v", local.cluster_name)
  network = var.vpc

  description = "GKE API to kubelets"

  source_ranges = [
    var.private_master_cidr
  ]

  allow {
    protocol = "tcp"
    ports    = [8443]
  }

  target_tags = setunion(
    [local.cluster_name],
    var.kubelet_firewall_tags,
  )
}
