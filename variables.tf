variable "authorized_master_access" {
  type        = list(string)
  description = "External networks that can access the Kubernetes cluster master."
  default = [
    "0.0.0.0/0"
  ]
}

variable "environment" {
  type        = string
  description = "The environment name."
}

variable "horizontal_pod_autoscaling" {
  type        = bool
  description = "Enable horizontal pod autoscaling addon"
  default     = true
}

variable "http_load_balancing" {
  type        = bool
  description = "Enable http load balancer addon"
  default     = true
}

variable "network_policy" {
  type        = bool
  description = "Enable network policy addon"
  default     = true
}

variable "network_policy_provider" {
  type        = string
  description = "The network policy provider."
  default     = "CALICO"
}

variable "datapath_provider" {
  type        = string
  description = "The desired datapath provider for this cluster. By default, `DATAPATH_PROVIDER_UNSPECIFIED` enables the IPTables-based kube-proxy implementation. `ADVANCED_DATAPATH` enables Dataplane-V2 feature."
  default     = "DATAPATH_PROVIDER_UNSPECIFIED"
}

variable "ip_cidr_range" {
  type        = string
  description = "The IP range for the cluster."
}

variable "pod_ip_cidr_range" {
  type        = string
  description = "Secondary IP range for pods. /16 == /24 per node = 256 nodes"
  default     = "10.12.0.0/16"
}

variable "service_ip_cidr_range" {
  type        = string
  description = "Secondary IP range for services. /22 = 1024 service IPs"
  default     = "10.16.0.0/22"
}

variable "release_channel" {
  type        = string
  description = "The release channel of this cluster. Accepted values are UNSPECIFIED, RAPID, REGULAR and STABLE. Defaults to UNSPECIFIED."
  default     = "UNSPECIFIED"
}

variable "kubernetes_version" {
  type        = string
  description = "The Kubernetes version. Defaults to latest available."
  default     = ""
}

variable "oauth_scopes" {
  type        = list(string)
  description = "Default set of Google API scopes to enable in all node pools."
  default = [
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring",
  ]
}

variable "service_account" {
  type        = string
  description = "Overrides the service account managed by this module. Will apply the designated service account defined by this variable."
  default     = null
}

variable "name" {
  type        = string
  description = "The GKE cluster name, `name` or `name_prefix` required."
  default     = null
}

variable "name_prefix" {
  type        = string
  description = "The GKE cluster name prefix, conflicts with `name`, generates a random cluster name `<name_prefix>-<random_id>`."
  default     = null
}

variable "private_cluster" {
  type        = bool
  description = "Whether or not a cluster should be private or not."
  default     = true
}

variable "private_cluster_endpoint" {
  type        = bool
  description = "When true, the cluster's private endpoint is used as the cluster endpoint and access through the public endpoint is disabled. When false, either endpoint can be used."
  default     = false
}

variable "private_master_cidr" {
  type        = string
  description = "Master IP range is a private RFC 1918 range for the master's VPC. The master range must not overlap with any subnet in your cluster's VPC. The master and your cluster use VPC peering to communicate privately."
  default     = "172.16.0.0/28"
}

variable "project" {
  type        = string
  description = "The project name."
}

variable "region" {
  type        = string
  description = "The target region."
}

variable "regional_cluster" {
  type        = bool
  description = "Creates a cluster with multiple masters spread across zones in the region; forces new resource when changed."
  default     = true
}

variable "zone" {
  type        = string
  description = "For zonal cluster, explicitly set the zone for the cluster and node pools."
  default     = ""
}

variable "regional_cluster_zones" {
  type        = list(string)
  description = "For a regional cluster, specify an explicit list of zones to place nodes. An empty list will default to all available zones."
  default     = []
}

variable "vpc" {
  type        = string
  description = "The target network."
}

# name - Provide a name for the node pool to be created, must be unique
# initial_node_count - set the starting size of the node_pool per zone, setting this to 0, creates an inoperable node_pool
# machine_type - The type of VM for the worker nodes
# min_node_count - The minimum number nodes per zone
# max_node_count - The maximum number nodes per zone
# node_auto_repair - Whether the nodes will be automatically repaired
# node_auto_upgrade - Whether the nodes will be automatically upgraded
# node_image_type - The image type to use for this node. Forces new resource.
# node_disk_size - Size of the disk attached to each node, specified in GB. Forces new resource.
# node_disk_type - Type of the disk attached to each node. Forces new resource.
# spot - Use spot instances for this node pool
# kubernetes_version - The version of Kubernetes to use for this node pool. If not specified, it defaults to the value of kubernetes_version

variable "node_pools" {
  type = map(object({
    cpu_cfs_quota            = optional(bool)
    cpu_cfs_quota_period     = optional(string)
    cpu_manager_policy       = optional(string)
    enable_workload_identity = optional(bool)
    node_locations           = optional(list(string))
    initial_node_count       = optional(number, 1)
    kubernetes_version       = optional(string)
    labels                   = optional(map(string), {})
    location_policy          = optional(string)
    machine_type             = optional(string, "n2d-standard-8")
    max_node_count           = optional(number, 10)
    max_surge                = optional(number, 1)
    max_unavailable          = optional(number, 0)
    min_node_count           = optional(number, 1)
    node_auto_repair         = optional(bool, true)
    node_auto_upgrade        = optional(bool, false)
    node_disk_size_gb        = optional(number, 100)
    node_disk_type           = optional(string, "pd-ssd")
    node_image_type          = optional(string, "COS_CONTAINERD")
    oauth_scopes             = optional(list(string), [])
    shard                    = optional(string)
    spot                     = optional(bool, false)
    stage                    = optional(string)
    taints                   = optional(list(object({ key = string, value = string, effect = string })), [])
    type                     = optional(string, "default")
  }))
  description = "Map of node pools with their configuration."
  default = {
    default = {}
  }
}

###############################################
# IAP netblock to allow TCP forwarding
# for ssh access, see https://cloud.google.com/iap/docs/using-tcp-forwarding
variable "enable_cloud_iap_access" {
  type        = bool
  description = "Determines whether to create a firewall rule for TCP forwarding from IAP."
  default     = true
}

variable "cloud_iap_netblock" {
  type        = string
  description = "IP range of the IAP."
  default     = "35.235.240.0/20"
}

###############################################

variable "enable_workload_identity" {
  type        = bool
  description = "Enable GKE Workload Identity support on cluster. See https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity"
  default     = true
}

variable "workload_identity_ksa" {
  type        = map(list(string))
  description = "All Kubernetes service accounts that must utilize Workload Identity. Formatted as `{'<namespace>/<service_account>': ['<iam_role>', ...], ...}`."
  default     = {}
}

variable "enable_vertical_pod_autoscaling" {
  type        = bool
  description = "Enable GKE vertical pod autoscaling. See https://cloud.google.com/kubernetes-engine/docs/concepts/verticalpodautoscaler"
  default     = true
}

variable "network_egress_metering_enabled" {
  type        = bool
  description = "Whether to enable network egress metering for this cluster; requires `resource_usage_dataset` to be configured."
  default     = false
}

variable "resource_consumption_metering_enabled" {
  type        = bool
  description = "Whether to enable resource consumption metering on this cluster; requires `resource_usage_dataset` to be configured."
  default     = false
}

variable "resource_usage_dataset" {
  type        = string
  description = "The ID of a BigQuery Dataset, required if resource usage is enabled."
  default     = ""
}

variable "maintenance_exclusions" {
  type        = list(object({ name = string, start_time = string, end_time = string }))
  description = "List of maintenance exclusions for the cluster (up to 3). See https://www.terraform.io/docs/providers/google/r/container_cluster.html#maintenance_policy"
  default     = []

  validation {
    condition     = length(var.maintenance_exclusions) <= 3
    error_message = "You can only have up to 3 maintenance exclusion windows."
  }
}

variable "maintenance_policy" {
  type = object({
    start_time = string
    end_time   = string
    recurrence = string
  })
  description = "The maintenance policy for the cluster. See https://www.terraform.io/docs/providers/google/r/container_cluster.html#maintenance_policy"
  default     = null
}

variable "create_nat" {
  type        = bool
  description = "Whether or not to create a NAT."
  default     = false
}

variable "subnet_nat_name" {
  type        = string
  description = "Name of the network subnet and nat object that is created. Defaults to `gke-{local.cluster_name}-{var.environment}`."
  default     = null
}

variable "nodes_service_account_name" {
  type        = string
  description = "Name of the gke nodes service account that is created. Defaults to `gke-{local.cluster_name}-nodes`."
  default     = null
}

variable "create_kublet_firewall" {
  type        = bool
  description = "Create firewall to allow GKE to talk to the Prometheus operator."
  default     = false
}

variable "kubelet_firewall_tags" {
  type        = set(string)
  description = "Extra firewall target tags for inbound control plane and SSH traffic."
  default     = []
}

variable "upgrade_notification_queue" {
  type        = string
  description = "The pubsub topic to send messages to when a cluster upgrade takes place. This must be a full URL path to the topic."
  default     = null
}

variable "enable_shielded_nodes" {
  type        = bool
  description = "Enable Shielded Nodes features on all nodes in this cluster."
  default     = true
}

variable "enable_l4_ilb_subsetting" {
  type        = bool
  description = "Enable L4 ILB Subsetting on the cluster"
  default     = false
}

variable "gce_pd_csi_driver" {
  type        = bool
  description = "Whether this cluster should enable the Google Compute Engine Persistent Disk Container Storage Interface (CSI) Driver. https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/gce-pd-csi-driver"
  default     = false
}

variable "logging_enabled_components" {
  type        = list(string)
  description = "List of services to monitor: SYSTEM_COMPONENTS, WORKLOADS. Empty list is default GKE configuration."
  default     = []
}

variable "monitoring_enabled_components" {
  type        = list(string)
  description = "List of services to monitor: SYSTEM_COMPONENTS, WORKLOADS (provider version >= 3.89.0). Empty list is default GKE configuration."
  default     = []
}

variable "monitoring_enable_managed_prometheus" {
  type        = bool
  description = "(Beta) Configuration for Managed Service for Prometheus. Whether or not the managed collection is enabled."
  default     = false
}

variable "node_pools_linux_node_configs_sysctls" {
  type        = map(map(string))
  description = "Map of maps containing linux node config sysctls by node-pool name"

  # Default is being set in variables_defaults.tf
  default = {
    all               = {}
    default-node-pool = {}
  }
}

variable "config_connector" {
  type        = bool
  description = "(Beta) Whether ConfigConnector is enabled for this cluster."
  default     = false
}

variable "cluster_resource_labels" {
  type        = map(string)
  description = "The GCE resource labels (a map of key/value pairs) to be applied to the cluster"
  default     = {}
}

variable "enable_cost_allocation" {
  type        = bool
  description = "Enables Cost Allocation Feature and the cluster name and namespace of your GKE workloads appear in the labels field of the billing export to BigQuery"
  default     = false
}

variable "disable_default_snat" {
  type        = bool
  description = "Whether to disable the default SNAT to support the private use of public IP addresses"
  default     = false
}
